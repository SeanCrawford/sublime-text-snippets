Sublime Text 2 Snippets
=======================

Clone this folder into:
	/Users/*YOUR_USERNAME*/Library/Application Support/Sublime Text 2/Packages/User

HTML
----
	Misc
		html5 			: creates a HTML5 document structure with title, viewport, script and link.

CSS
---
	Misc
		base 			: all the main text based tags to define type hierarchy
		mq 				: starting point for a media query

JS
--
	Quintus
		quintus 	 	: a starting point for a quintus project (index.html)
		quintusload  	: load function to begin game which includes sprite loading and initializing first 'scene'
		quintusobject	: extending a sprite or existing quintus object
		quintusscene 	: create a new quintus 'scene'

	Backbone
		bbmodel 		: starting point for a backbone model
		bbcollection	: starting point for a backbone collection (list of models)
		bbview 			: starting point for a backbone view loading data from a model
		bbviews 		: starting point for a backbone view loading data from a collection

	Other
		ajax 			: jquery ajax function with response structure
		post 			: jquery post function
		jsobject 		: starting point for a javascript object
		fc 				: function with a javascript object
		plugin 			: starting point for jquery plugin
		log 			: quick console log
